﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestWinformApp
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtID.Text == string.Empty)
            {
                MessageBox.Show("Tài khoản không được để trống");
                txtID.Focus();
            }
            else if (txtPassword.Text == string.Empty)
            {
                MessageBox.Show("Mật khẩu không được để trống");
                txtPassword.Focus();
            }
            else
            {
                string _StrID = txtID.Text;
                string _StrPassword = txtPassword.Text;

                bool _BCheckID = true;
                for (int i = 0; i < _StrID.Length; i++)
                {
                    if (_StrID[i] < '0' || _StrID[i] > '9')
                    {
                        _BCheckID = false;
                        break;
                    }
                }

                if (_BCheckID)
                {
                    MessageBox.Show("Đăng nhập thành công\n" + "ID: " + _StrID + " \nPassword: " + _StrPassword);
                }
                else
                {
                    MessageBox.Show("Tài khoản chỉ được nhập số");
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
